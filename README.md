# Spring Boot With H2 Database and kubernetes application

prerequisites: 

* Basic knowledge of java, kubernetes
* Install kubernetes cluster

#### Reusing the Docker daemon of minikube. run the following command from terminal

```
    $ eval $(minikube docker-env)
    
    $ docker ps
```


The easiest way to setup the Maven Wrapper for your project is to use the Maven Wrapper Plugin with its provided wrapper goal. To add or update all the necessary Maven Wrapper files to your project execute the following command:
```
    $ mvn wrapper:wrapper
```
You can then build the application:
```   
    $./mvnw install
```
### Containerize the Application

run the following command in project directory
```
    $ ./mvnw spring-boot:build-image
```
You can run the container locally:
```
    $ docker run -p 8080:8080 spring-boot-h2-db-web-app:0.0.1-SNAPSHOT
```
Finish by stopping the container.


You cannot push the image unless you authenticate with Dockerhub (docker login), but there is already an image there that should work. If you were authenticated, you could:
```
    $ docker login -u dockerhub_username
    $ docker tag spring-boot-h2-db-web-app:0.0.1-SNAPSHOT  cloudeclipse/springbootguides
    $ docker push cloudeclipse/springbootguides
```    
In real life, the image needs to be pushed to Dockerhub (or some other accessible repository) because Kubernetes pulls the image from inside its Kubelets (nodes), which are not usually connected to the local docker daemon. For the purposes of this scenario, you can omit the push and use the image that is already there.

For testing, there are workarounds that make docker push work with an insecure local registry (for instance) but that is out of scope for this guide.


## Deploy the Application to Kubernetes

Now you have a container that runs and exposes port 8080, so all you need to make Kubernetes run it is some YAML. To avoid having to look at or edit YAML, for now, you can ask kubectl to generate it for you. The only thing that might vary here is the --image name. If you deployed your container to your own repository, use its tag instead of this one:
```
    $ kubectl create deployment spring-boot-h2-db-web-app --image=springbootguides/spring-boot-h2-db-web-app --dry-run -o=yaml > deployment.yaml
    
    $ echo --- >> deployment.yaml
    
    $ kubectl expose deploy spring-boot-h2-db-web-app --port=8080 --target-port=8080 --name=spring-boot-lb --type=LoadBalancer
```
You can take the YAML generated above and edit it if you like, or you can apply it as is:
```
    $ kubectl apply -f deployment.yaml
```
Check that the application is running:
```
    Rathores-MacBook-Pro:spring-boot-h2-db-web-app rathore$ k get all
    NAME                                            READY   STATUS    RESTARTS   AGE
    pod/spring-boot-h2-db-web-app-bfff78b55-85kcr   1/1     Running   0          178m
    
    NAME                     TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
    service/kubernetes       ClusterIP      10.96.0.1       <none>        443/TCP          205d
    service/spring-boot-lb   LoadBalancer   10.111.30.120   <pending>     8080:31416/TCP   15m
    
    NAME                                        READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/spring-boot-h2-db-web-app   1/1     1            1           3h1m
    
    NAME                                                  DESIRED   CURRENT   READY   AGE
    replicaset.apps/spring-boot-h2-db-web-app-5b4569f9b   0         0         0       3h1m
    replicaset.apps/spring-boot-h2-db-web-app-bfff78b55   1         1         1       178m
```
Repeat kubectl get all until the demo pod shows its status as Running.
Now you need to be able to connect to the application, which you have exposed as a Service in Kubernetes. One way to do that, which works great at development time, is to create an SSH tunnel:
```
    $ kubectl port-forward svc/spring-boot-h2-db-web-app 8080:8080	
```
Then you can verify that the app is running in another terminal:
```
    $ curl localhost:8080/customers
```
If you are using minikube execute below command
```
    Rathores-MacBook-Pro:spring-boot-h2-db-web-app rathore$ minikube service spring-boot-lb
    |-----------|----------------|-------------|---------------------------|
    | NAMESPACE |      NAME      | TARGET PORT |            URL            |
    |-----------|----------------|-------------|---------------------------|
    | default   | spring-boot-lb |        8080 | http://192.168.64.2:31416 |
    |-----------|----------------|-------------|---------------------------|
    🎉  Opening service default/spring-boot-lb in default browser...
```    
open url in browser: http://192.168.64.2:8080/customers


references: 
* https://spring.io/guides/gs/spring-boot-kubernetes/
* https://www.section.io/engineering-education/spring-boot-kubernetes/ 